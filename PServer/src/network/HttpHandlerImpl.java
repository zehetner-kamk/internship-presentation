package network;

import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class HttpHandlerImpl implements com.sun.net.httpserver.HttpHandler {

    @Override
    public void handle(HttpExchange httpExchange) throws IOException { //gets called when http request is made to server
        StringBuilder sb = new StringBuilder();
        Files.lines(Paths.get("src/index.html")).forEach(sb::append);
        String html = sb.toString();

        if (httpExchange.getRequestMethod().equals("GET")) {
            httpExchange.sendResponseHeaders(200, html.length() + 4);
            httpExchange.getResponseBody().write(html.getBytes(StandardCharsets.UTF_8));
        } else {
            httpExchange.sendResponseHeaders(400, 0);
        }

        httpExchange.close();
    }


}
